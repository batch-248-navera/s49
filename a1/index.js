

fetch('https://jsonplaceholder.typicode.com/posts').then((res)=>res.json()).then((data)=>showPosts(data));

document.querySelector("#form-add-post").addEventListener('submit',(e)=>{
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts',
    {
        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector("#text-title").value,
            body: document.querySelector("#text-body").value,
            userId: 1
        }),
        headers:{'Content-Type':'application/json; charset=UTF-8'}
    })
    .then((res)=>res.json())
    .then((data)=>{
        
        
        const createPost = document.createElement('div');
        createPost.setAttribute('id', `post-${data.userId}`);

        
        const createPostInnerHTML = `
            <h3 id="post-title-${data.userId}">${data.title}</h3>
            <p id="post-body-${data.userId}">${data.body}</p>
            <button onclick="editPost('${data.userId}')">Edit</button>
            <button onclick="deletePost('${data.userId}')">Delete</button>
        `;

       
        createPost.innerHTML = createPostInnerHTML;

        
        document.querySelector('#div-post-entries').insertBefore(createPost, document.querySelector('#div-post-entries').firstChild);


        alert('Successfully added.')


        document.querySelector("#text-title").value = null;
        document.querySelector("#text-body").value = null;
    })
})

const showPosts = (posts) =>{

    let postEntries = "";

    posts.forEach((post)=>{

        postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
    })

    document.querySelector("#div-post-entries").innerHTML = postEntries;
}


const editPost = (id) =>{

    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#text-edit-id").value = id;
    document.querySelector("#text-title-edit").value = title;
    document.querySelector("#text-body-edit").value = body;
    document.querySelector("#edit-btn").removeAttribute('disabled');

    document.querySelector("#form-edit-post").addEventListener('submit',(e)=>{
        e.preventDefault();

        fetch('https://jsonplaceholder.typicode.com/posts/1',
        {
            method: 'PUT',
            body: JSON.stringify({
                title: document.querySelector("#text-edit-id").value,
                title: document.querySelector("#text-title-edit").value,
                body: document.querySelector("#text-body-edit").value,
                userId: 1
            }),
            headers:{'Content-Type':'application/json; charset=UTF-8'}
        })
        .then((res)=>res.json())
        .then((data)=>{
            console.log(data);
            alert('Successfully updated.')
    
            document.querySelector("#text-edit-id").value = null;
            document.querySelector("#text-title-edit").value = null;
            document.querySelector("#text-body-edit").value = null;
            document.querySelector("#edit-btn").setAttribute('disabled');

        })
    })
}
const deletePost = (id) =>{
        
       document.querySelector(`#post-${id}`).remove();
           
}

